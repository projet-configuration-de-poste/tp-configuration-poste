package fr.aej.tpconfigposte.services;

import fr.aej.tpconfigposte.data.entities.RecipeEntity;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.jasperreports.JasperReportsUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PdfService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfService.class);

    private final String PDF_TEMPLATE = "/recipes.jrxml";

    public File generatePdf(RecipeEntity recipe) throws IOException {
        File pdfFile = File.createTempFile("recipesBook", ".pdf");

        try (FileOutputStream fos = new FileOutputStream(pdfFile)) {
            final JasperReport report = loadTemplate();
            final Map<String, Object> parameters = parameters(recipe);

            final JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Collections.singletonList("RecipeBook"));

            JasperReportsUtils.renderAsPdf(report, parameters, dataSource, fos);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return pdfFile;
    }

    public JasperReport loadTemplate() throws JRException {
        final InputStream input = getClass().getResourceAsStream(PDF_TEMPLATE);
        final JasperDesign jasperDesign = JRXmlLoader.load(input);

        return JasperCompileManager.compileReport(jasperDesign);
    }

    public Map<String, Object> parameters(RecipeEntity recipe) {
        return new HashMap<String, Object>() {
            { put("recipe", recipe); }
        };
    }
}
