package fr.aej.tpconfigposte.services;

import fr.aej.tpconfigposte.data.entities.RecipeEntity;
import fr.aej.tpconfigposte.data.repositories.IRecipesRepository;
import fr.aej.tpconfigposte.services.interfaces.IRecipesService;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class RecipesService implements IRecipesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecipesService.class);

    @Autowired
    private IRecipesRepository repository;

    @Override
    public List<RecipeEntity> getRecipes() {
        LOGGER.info("Get Recipes");
        return this.repository.findAll();
    }

    @Override
    public File generatePdf() throws IOException {
        List<RecipeEntity> recipes = this.getRecipes();

        PdfService pdf = new PdfService();
        return pdf.generatePdf(recipes.get(0));
    }
}
