package fr.aej.tpconfigposte.services.interfaces;

import fr.aej.tpconfigposte.data.entities.RecipeEntity;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface IRecipesService {
    List<RecipeEntity> getRecipes();
    File generatePdf() throws IOException;
}
