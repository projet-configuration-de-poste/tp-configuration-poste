-- Ingredient data set
INSERT INTO ingredient_entity(id, title) VALUES(1, 'Tomate');
INSERT INTO ingredient_entity(id, title) VALUES(2, 'Crevette');
INSERT INTO ingredient_entity(id, title) VALUES(3, 'Mozzarella');

-- Recipe data set
INSERT INTO recipe_entity(id, title, description) VALUES(1, 'Recette 1', 'description 1');
INSERT INTO recipe_entity(id, title, description) VALUES(2, 'Recette 2', 'description 2');
INSERT INTO recipe_entity(id, title, description) VALUES(3, 'Recette 3', 'description 3');

-- Recipe Ingredient data set
INSERT INTO recipe_entity_ingredients(recipe_entity_id, ingredients_id) VALUES(1, 1);
INSERT INTO recipe_entity_ingredients(recipe_entity_id, ingredients_id) VALUES(1, 2);
INSERT INTO recipe_entity_ingredients(recipe_entity_id, ingredients_id) VALUES(2, 1);
INSERT INTO recipe_entity_ingredients(recipe_entity_id, ingredients_id) VALUES(3, 3);