package fr.aej.tpconfigposte.data.repositories;

import fr.aej.tpconfigposte.data.entities.RecipeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRecipesRepository extends JpaRepository<RecipeEntity, Integer> {
}
