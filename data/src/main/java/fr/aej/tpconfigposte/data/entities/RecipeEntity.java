package fr.aej.tpconfigposte.data.entities;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class RecipeEntity {
    @Id
    @GeneratedValue
    @NotNull
    private Integer id;

    @NotNull
    @Column
    private String title;

    @NotNull
    @Column
    private String description;

    @OneToMany
    private List<IngredientEntity> ingredients;

    public RecipeEntity() {}
    public RecipeEntity(Integer id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public String getDescription() { return this.description; }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<IngredientEntity> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientEntity> ingredients) {
        this.ingredients = ingredients;
    }
}
