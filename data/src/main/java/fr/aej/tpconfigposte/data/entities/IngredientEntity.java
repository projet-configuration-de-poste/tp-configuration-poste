package fr.aej.tpconfigposte.data.entities;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class IngredientEntity {
    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    @Column
    private String title;

    public IngredientEntity() {}
    public IngredientEntity(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
