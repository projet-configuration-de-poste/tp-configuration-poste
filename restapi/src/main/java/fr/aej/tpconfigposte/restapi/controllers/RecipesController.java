package fr.aej.tpconfigposte.restapi.controllers;

import fr.aej.tpconfigposte.data.entities.RecipeEntity;
import fr.aej.tpconfigposte.services.interfaces.IRecipesService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

@RequestMapping("/recipes")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.OPTIONS, RequestMethod.POST})
public class RecipesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecipesController.class);

    @Autowired
    private IRecipesService recipesService;

    @CrossOrigin
    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity getAllRecipes() {
        LOGGER.info("GetAllRecipes method");

        List<RecipeEntity> recipes;

        try {
            recipes = this.recipesService.getRecipes();

            return ResponseEntity.ok(new HashMap<String, Object>() {
                { put("recipes", recipes); }
            });
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return new ResponseEntity<>(new HashMap<String, Object>() {
                { put("Message", ex.getMessage()); }
            }, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(path = "generateBook", method = RequestMethod.GET)
    public ResponseEntity getRecipesBook() throws IOException {
        File book = this.recipesService.generatePdf();

        byte[] contents = Files.readAllBytes(book.toPath());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        String filename = "RecipesBook.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);

        return response;
    }
}
