# Lancement de la dernier version du projet


## Service Java Spring boot

1. Cloner le projet
2. Se rendre dans le dossier deployment
3. executer la command : docker-compose up -d
4. Apres quelques secondes, le service est accessible via l'url localhost:8000

## Front-end

1. Cloner le projet https://gitlab.com/projet-configuration-de-poste/tp-configuration-poste-front.git
2. Builder l'application angular : npm run build
3. Deplacer le contenu du dossier dist/ dans le dossier deployment/html/front du projet Java Spring boot
4. Le front est accessible via l'url : http://localhost/front/

### Deployment

Lors du déploiement, le projet va lancer 3 container docker :
1. Apache (pour lancer le front angular)
2. Mariadd (avec un jeu de données de recettes et d'ingrédients)
3. Le service Spring Boot.

